#!/usr/bin/env bash

function run
{
  cd ./docker

  SOURCE_VOLUME=$([ -z $1 ] && echo $(dirname "$(pwd)") || echo ${1})

  echo "$SOURCE_VOLUME"

  docker run -d \
    -v $SOURCE_VOLUME/:/www/test \
    --name test \
    test:latest \
    tail -f /dev/null

  cd ..
}

function enter
{
  docker exec -it test /bin/ash
}

function build
{
  cd ./docker
  docker build -t test .
  cd ..
}

case "$1" in
 "build" | "b" )
    build
  ;;
  "run" | "r" )
    run
  ;;
  "enter" | "en" )
    enter
  ;;
esac